

FROM node:12 as build

# Create app directory
WORKDIR /usr/src/app

# Install app dependencies
# A wildcard is used to ensure both package.json AND package-lock.json are copied
# where available (npm@5+)
COPY package*.json ./

RUN npm install
# If you are building your code for production
# RUN npm ci --only=production

# Bundle app source
COPY . .

FROM alpine:latest

# Install necessary packages
RUN apk --no-cache add bind-tools npm

WORKDIR /usr/src/app

COPY --from=build /usr/src/app .

EXPOSE 3000

CMD [ "npm", "run", "start" ]
# ENTRYPOINT [ "tail", "-f", "/dev/null" ]